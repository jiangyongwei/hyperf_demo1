<?php

declare(strict_types=1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://hyperf.wiki
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */
return [
    Hyperf\Crontab\Process\CrontabDispatcherProcess::class, //配置定时任务组件进程
    Hyperf\AsyncQueue\Process\ConsumerProcess::class, //配置redis异步消费进程
];
