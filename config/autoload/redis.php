<?php

declare(strict_types=1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://hyperf.wiki
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */
return [
    'default' => [
        'host' => env('REDIS_HOST', 'localhost'),
        'auth' => env('REDIS_AUTH', null),
        'port' => (int) env('REDIS_PORT', 6379),
        'db' => (int) env('REDIS_DB', 0),
        'pool' => [
            'min_connections' => 1,
            'max_connections' => 10,
            'connect_timeout' => 10.0,
            'wait_timeout' => 3.0,
            'heartbeat' => -1,
            'max_idle_time' => (float) env('REDIS_MAX_IDLE_TIME', 60),
        ],
    ],
];


/*
配置项	类型	默认值	备注
host	string	'localhost'	Redis 地址
auth	string	无	密码
port	integer	6379	端口
db	integer	0	DB
cluster.enable	boolean	false	是否集群模式
cluster.name	string	null	集群名
cluster.seeds	array	[]	集群连接地址数组 ['host:port']
pool	object	{}	连接池配置
options	object	{}	Redis 配置选项
*/