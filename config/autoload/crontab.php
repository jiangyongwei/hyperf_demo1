<?php
use Hyperf\Crontab\Crontab;

return [
    'enable' => false,
    // 通过配置文件定义的定时任务
    'crontab' => [
        // Callback类型定时任务（默认）
        (new Crontab())->setName('Index')->setRule('*/5 * * * * *')->setCallback([App\Controller\IndexController::class, 'crontabTest'])->setMemo('这是一个示例的定时任务'),
        // Command类型定时任务
        /*(new Crontab())->setType('command')->setName('Bar')->setRule('* * * * *')->setCallback([
            'command' => 'swiftmailer:spool:send',
            // (optional) arguments
            'fooArgument' => 'barValue',
            // (optional) options
            '--message-limit' => 1,
        ]),*/
    ],
];