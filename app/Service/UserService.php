<?php

declare(strict_types=1);

namespace App\Service;

use App\Model\User as UserModel;
use Hyperf\DbConnection\Db;

class UserService {

    /**
     * 登录
     * @param array $params
     */
    public function login(array $params) {
        return ['age' => 18];
    }

    public function userInfo() {
        //判断当前是否处于协程环境内
        $res = \Hyperf\Utils\Coroutine::inCoroutine();
        var_dump($res); //bool(true)
        //获得当前协程的 ID
        $id = \Hyperf\Utils\Coroutine::id();
        var_dump($id); //2
        $list = UserModel::query()->from("user as u")->get()->toArray();
        $userInfo = UserModel::query()->where(['uid' => 12])->first();
        $userRaw = UserModel::query()->where('uid', '=', 12)->first();
        $userRaw = !empty($userRaw) ? $userRaw->toArray() : [];
        var_dump($userInfo);
        //var_dump($userRaw);

        $users = Db::table('user as u')->select('uid', 'phone')->get();
        var_dump($users->toArray());

        return $list;
    }
}
