<?php

declare(strict_types=1);

namespace App\Service;

use App\Common\RedisKey;
use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpServer\Contract\RequestInterface;
//use Hyperf\Redis\Redis;
use Hyperf\Utils\ApplicationContext;

class CheckTokenService {

    protected $request;

    public $user_id;
    public $mobile;

    public function __construct() {
        $this->checkToken();
    }

    /**
     * 检查用户token
     */
    private function checkToken() {
        $appEnv = env('APP_ENV');
        if (in_array($appEnv, ['local', 'test', 'prod'])) {
            $container = ApplicationContext::getContainer();
            $this->request = $container->get(RequestInterface::class);
            $userToken = $this->request->input('user_token');
            if (empty($userToken)) abort(0, 'user_token 不能为空');

            $redis = $container->get(\Hyperf\Redis\Redis::class);
            //$redis = make(Redis::class);
            $key = RedisKey::getUserToken($userToken);
            $userInfo = $redis->hGetAll($key);
            if(! $userInfo) abort(1061);
            if($userInfo['status'] != 1) abort(1060);
            $this->user_id = $userInfo['user_id'];
            $this->mobile = $userInfo['mobile'];
        }
    }

}
