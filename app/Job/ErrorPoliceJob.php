<?php

declare(strict_types=1);

namespace App\Job;

use Hyperf\AsyncQueue\Job;
use PHPMailer\PHPMailer\PHPMailer;

class ErrorPoliceJob extends Job
{
    public $params;

    /**
     * 任务执行失败后的重试次数，即最大执行次数为 $maxAttempts+1 次
     *
     * @var int
     */
    protected $maxAttempts = 2;

    public $to;

    public $subject;

    public $content;

    public function __construct($to, $subject, $content)
    {
        // 这里最好是普通数据，不要使用携带 IO 的对象，比如 PDO 对象
        $this->to = $to;
        $this->subject = $subject;
        $this->content = $content;
    }

    public function handle()
    {
        // 根据参数处理具体逻辑
        // 通过具体参数获取模型等
        // 这里的逻辑会在 ConsumerProcess 进程中执行
        $to = $this->to;
        $subject = $this->subject;
        $content = $this->content;
        var_dump($to, $subject, $content);

        go(function() use ($to, $subject, $content) {
            $mail = new PHPMailer; //PHPMailer对象
            $mail->CharSet = 'UTF-8'; //设定邮件编码，默认ISO-8859-1，如果发中文此项必须设置，否则乱码
            $mail->IsSMTP(); // 设定使用SMTP服务
            $mail->SMTPDebug = 0; // 关闭SMTP调试功能
            $mail->SMTPAuth = true; // 启用 SMTP 验证功能
            $mail->SMTPSecure = 'ssl'; // 使用安全协议
            $mail->Host = 'smtp.163.com'; // SMTP 服务器
            $mail->Port = 465; // SMTP服务器的端口号
            $mail->Username = '13286707639@163.com'; // SMTP服务器用户名
            $mail->Password = 'wei2017'; // SMTP服务器密码
            $mail->SetFrom('13286707639@163.com', 'JiangYongWei@163'); // 邮箱，昵称
            $mail->Subject = $subject;
            $mail->MsgHTML($content);
            //$mail->AddAddress('1085562810@qq.com'); // 收件人
            if(is_array($to)){
                foreach ($to as $v){
                    $mail->AddAddress($v);
                }
            }else{
                $mail->AddAddress($to);
            }
            //$mail->AddAddress($to); // 收件人

            //$mail->addCC('cc@example.com');                    //抄送
            //$mail->addBCC('bcc@example.com');                    //密送

            //发送附件
            // $mail->addAttachment('../xy.zip');         // 添加附件
            // $mail->addAttachment('../thumb-1.jpg', 'new.jpg');    // 发送附件并且重命名
            $result = $mail->Send();
            if($result)
            {
                var_dump('ok');
            }
            else
            {
                $result = $error = $mail->ErrorInfo;
                var_dump($result);
            }
        });

    }
}
