<?php

/**
 * 按文件名写入日志
 * @param string $filename 文件名称
 * @param string $log_msg 信息
 * @param mixed $data 数据
 * @return boolean
 */
function write_log(string $filename, string $log_msg, $data)
{
    if(is_array($data)) {
        //JSON_UNESCAPED_UNICODE 显示中文 | JSON_UNESCAPED_SLASHES 不转义反斜杠
        $data = json_encode($data, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
    }

    $path = BASE_PATH .'/runtime/';
    if(!file_exists($path)){
        @mkdir($path,0755);
    }
    $date_fmt = date('Y-m-d H:i:s',time());
    $filepath = $path.$filename.'.log';
    if(!$fp = @fopen($filepath, 'ab')){
        return false;
    }
    $message = $date_fmt.'|'.$log_msg.'|'.$data."\n";
    flock($fp, LOCK_EX);
    fwrite($fp, $message);
    flock($fp, LOCK_UN);
    fclose($fp);
    @chmod($filepath, 0755);
    return true;
}

/**
 * 按日期格式写入日志
 * @param string $filename 文件名称
 * @param string $log_msg 信息
 * @param mixed $data 数据
 * @return boolean
 */
function zlog(string $log_msg, $data)
{
    if(is_array($data)) {
        $data = json_encode($data, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES); //JSON_UNESCAPED_UNICODE 显示中文 | JSON_UNESCAPED_SLASHES 不转义反斜杠
    }
    $path = BASE_PATH . '/runtime/logs/' . date("Y-m");
    if (!is_dir($path)) {
        @mkdir($path,0755, true);
    }
    $filepath = $path . '/' . date("Y-m-d") . '.log';
    if(!$fp = @fopen($filepath, 'ab')){
        return false;
    }
    $date_fmt = date('Y-m-d H:i:s',time());
    $message = $date_fmt.'|'.$log_msg.'|'.$data."\n";
    flock($fp, LOCK_EX);
    fwrite($fp, $message);
    flock($fp, LOCK_UN);
    fclose($fp);
    @chmod($filepath, 0755);
    return true;
}

/**
 * 业务异常abort中止
 * @param string $message
 * @param number $code
 */
function abort($code = 0, $msg = null)
{
    throw new \App\Exception\FooException($code, $msg);
}

/**
 * 通用化API接口数据输出
 * @param int $code 业务状态码
 * @param string $msg 信息提示
 * @param array $data  数据
 */
/*function json($data = [], $code = 200, $msg = '', ResponseInterface $response) : Psr7ResponseInterface
{
    $res = [
        'code' => $code,
        'msg'  => $msg,
        //'data' => $data ? $data : new \stdClass(),
        'data' => is_array($data) && empty($data) ? (object)$data : $data,
    ];
    return $response->json($res);
}*/


