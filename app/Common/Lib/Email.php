<?php
namespace App\Common\Lib;

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require BASE_PATH.'/library/PHPMailer/src/Exception.php';
require BASE_PATH.'/library/PHPMailer/src/PHPMailer.php';
require BASE_PATH.'/library/PHPMailer/src/SMTP.php';

/**
 * 邮箱发送类
 */
class Email {

    public function send($to = '', $subject = '', $content = '') {
        $mail = new PHPMailer(true);                              // Passing `true` enables exceptions

        try {
            //服务器配置
            $mail->CharSet ="UTF-8";                     //设定邮件编码
            $mail->SMTPDebug = 0;                        // 调试模式输出
            $mail->isSMTP();                             // 使用SMTP
            $mail->Host = 'smtp.163.com';                // SMTP服务器
            $mail->SMTPAuth = true;                      // 允许 SMTP 认证
            $mail->Username = '13286707639@163.com';                // SMTP 用户名  即邮箱的用户名
            $mail->Password = 'wei2017';             // SMTP 密码  部分邮箱是授权码(例如163邮箱)
            $mail->SMTPSecure = 'ssl';                    // 允许 TLS 或者ssl协议    qq邮箱用ssl, 163邮箱用tls
            $mail->Port = 465;                            // 服务器端口 25 或者465 具体要看邮箱服务器支持

            $mail->setFrom('13286707639@163.com', 'JiangYongWei@163');  //发件人
            //$mail->addAddress($to);  // 收件人
            if(is_array($to)){
                foreach ($to as $v){
                    $mail->addAddress($v);
                }
            }else{
                $mail->addAddress($to);
            }

            //$mail->addAddress('ellen@example.com');  // 可添加多个收件人
            $mail->addReplyTo('13286707639@163.com', 'JiangYongWei@163'); //回复的时候回复给哪个邮箱 建议和发件人一致
            //$mail->addCC('cc@example.com');                    //抄送
            //$mail->addBCC('bcc@example.com');                    //密送

            //发送附件
            // $mail->addAttachment('../xy.zip');         // 添加附件
            // $mail->addAttachment('../thumb-1.jpg', 'new.jpg');    // 发送附件并且重命名

            //Content
            $mail->isHTML(true);                                  // 是否以HTML文档格式发送  发送后客户端可直接显示对应HTML内容
            $mail->Subject = $subject;//'蜂鸟邮箱验证码';
            //$mail->Body    = '<h1>验证码：</h1>' . $code;
            //$mail->AltBody = '蜂鸟邮箱验证码：'.$code;
            $mail->msgHTML($content);

            $res = $mail->send();
            var_dump($res);
            if($res) return array('status' => 1, 'msg' => '邮件发送成功');
            return array('status' => 0 , 'msg' => '邮件发送失败');
            //echo '邮件发送成功';
        } catch (Exception $e) {
            //echo '邮件发送失败: ', $mail->ErrorInfo;
            return array('status' => 0 , 'msg' => '邮件发送失败');
        }
    }
}