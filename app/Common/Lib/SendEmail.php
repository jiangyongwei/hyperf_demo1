<?php
declare(strict_types=1);

namespace App\Common\Lib;

use Hyperf\Utils\Context;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

class SendEmail
{

    /**
     * 发送邮箱
     * @param string $to
     * @param string $subject
     * @param string $content
     * @return mixed
     */
    public function send($to = '', $subject = '', $content = '')
    {
        $channel = new \Swoole\Coroutine\Channel();
        co(function() use ($channel, $to, $subject, $content) {
            $mail = new PHPMailer(true); //PHPMailer对象
            $mail->CharSet = 'UTF-8'; //设定邮件编码，默认ISO-8859-1，如果发中文此项必须设置，否则乱码
            $mail->IsSMTP(); // 设定使用SMTP服务
            $mail->SMTPDebug = 0; // 关闭SMTP调试功能
            $mail->SMTPAuth = true; // 启用 SMTP 验证功能
            $mail->SMTPSecure = 'ssl'; // 使用安全协议
            $mail->Host = 'smtp.163.com'; // SMTP 服务器
            $mail->Port = 465; // SMTP服务器的端口号
            $mail->Username = '13286707639@163.com'; // SMTP服务器用户名
            $mail->Password = 'wei2017'; // SMTP服务器密码
            $mail->SetFrom('13286707639@163.com', 'JiangYongWei@163'); // 邮箱，昵称
            $mail->Subject = $subject;
            $mail->MsgHTML($content);
            if(is_array($to)){
                foreach ($to as $v){
                    $mail->AddAddress($v);
                }
            }else{
                $mail->AddAddress($to);
            }
            //$mail->AddAddress($to); // 收件人

            //$mail->addCC('cc@example.com');                    //抄送
            //$mail->addBCC('bcc@example.com');                    //密送

            //发送附件
            // $mail->addAttachment('../xy.zip');         // 添加附件
            // $mail->addAttachment('../thumb-1.jpg', 'new.jpg');    // 发送附件并且重命名
            $result = $mail->Send();
            if($result)
            {
                var_dump('ok');
            }
            else
            {
                $error = $mail->ErrorInfo;
                var_dump($error);
            }
            $channel->push($result);
        });
        return $channel->pop();

    }

}