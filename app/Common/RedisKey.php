<?php
namespace App\Common;

class RedisKey {
    //10秒
    const tenSeconds = 10;
    //20秒
    const twentySeconds = 20;
    //60秒   
    const sixtySeconds = 60;
    //10分钟
    const tenMinute = 600;
    //一天
    const oneDay = 86400;
    //两天
    const twoDay = 172800;
    
    //获取验证码
    static public function getCode($phone_email) {
        return 'hyperf:code:'.$phone_email;
    }
    
    //获取验证码账号或ip限制
    static public function getCodeLimit($username_ip) {
        return 'hyperf:code_limit:'.$username_ip;
    }
    
    //获取用户id
    static public function getUserId($user_id) {
        return 'hyperf:user_info:'.$user_id;
    }
    
    //获取用户token
    static public function getUserToken($user_token) {
        return 'hyperf:user:'.$user_token;
    }
    
    //获取vendor_user token
    static public function vendorUserToken($token) {
        return 'hyperf:vendor_user:'.$token;
    }
    
    //获取api_auth
    static public function getApiAuth($api_key) {
        return 'api_auth:'.$api_key;
    }
    
    //获取图形验证码
    static public function getCaptcha($captcha_sign) {
        return 'hyperf:captcha_code:'.$captcha_sign;
    }
    
    //获取积分防刷限制
    static public function getIntegralLimit($type, $key) {
        return 'hyperf:'.$type.':'.$key;
    }
    
    //防止重复点击权限等级下单
    static public function getAddOrder($user_id) {
        return 'hyperf:add_order:'.$user_id;
    }
    
    //防止重复点击支付
    static public function getPayment($user_id) {
        return 'hyperf:payment:'.$user_id;
    }
    
    //防止重复点击取消订阅
    static public function getCancelSubscribe($user_id) {
        return 'hyperf:cancel_subscribe:'.$user_id;
    }
    
    //获取广告列表
    static public function getBannerList($position) {
        return 'hyperf:banner:list:'.$position;
    }
    //获取api列表
    static public function getApiList() {
        return 'hyperf:api:list';
    }
    //获取积分配置列表
    static public function getIntegralConfigList() {
        return 'hyperf:integral_config:list';
    }
    //获取社区文章列表
    static public function getArticleList($type, $page) {
        return 'hyperf:article:list:'.$type.':'.$page;
    }
    
    //获取社区文章详情
    static public function getArticleDetail($id) {
        return 'hyperf:article:detail:'.$id;
    }
    
    //获取深度文章列表
    static public function getDepthArticleList($category, $max_id, $is_recommend, $page) {
        return 'hyperf:depth_article:list:'.$category.':'.$max_id.':'.$is_recommend.':'.$page;
    }
    
    //下单太频繁限制
    static public function addOrderLimit($user_id) {
        return 'hyperf:add_order_limit:'.$user_id;
    }
    
    //蜂鸟bee_securitykey、fc钱包fc_securitykey、win交易所win_securitykey
    static public function securitykey() {
        return 'hyperf:securitykey';
    }
    
    //币种人民币价格
    static public function rmbPrice() {
        return 'hyperf:rmb_price';
    }
    
    //GYBMember支付金额89
    static public function payAmountGYBMember() {
        return 'hyperf:pay_amount_gyb_member';
    }
    
}