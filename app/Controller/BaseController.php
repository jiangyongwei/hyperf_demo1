<?php

declare(strict_types=1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://hyperf.wiki
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */
namespace App\Controller;

use Hyperf\HttpServer\Contract\RequestInterface;
use Hyperf\Utils\Context;

class BaseController extends AbstractController
{
    public function __construct()
    {
        $this->check();
    }

    public function check() {
        $request = Context::get(RequestInterface::class, $this->request);
        $allParams = $request->all();
        var_dump($allParams);
        return 123;
        zlog('request_params:请求参数', $allParams);
        $action = $request->getRequestUri(); //获取访问的文件路径 http://192.168.31.163:9501/home/index/index?age=1&name=jiang  => /home/index/index
        if(! isset($allParams['sign']) || empty($allParams['sign'])){
            return json([], 0, '签名参数错误');
        }
    }

}
