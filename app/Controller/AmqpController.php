<?php
namespace App\Controller;

use Hyperf\HttpServer\Annotation\Controller;
use Hyperf\HttpServer\Annotation\RequestMapping;
use Hyperf\Amqp\Producer;
use App\Amqp\Producer\DemoProducer;
use Hyperf\Utils\ApplicationContext;

/**
 * 用户
 * @Controller(prefix="home/amqp")
 */
class AmqpController {

    /**
     * 登录
     * @author yongwei
     * @RequestMapping(path="test")
     */
    public function test()
    {
        $data = [
            'code' => 200,
            'data' => [
                'userOutName' => 'ccflow',
                'userOutNum' => '9999',
                'recordOutTime' => date("Y-m-d H:i:s", time()),
                'doorOutName' => '教师公寓',
            ]
        ];
        $json = json_encode($data);
        $message = new DemoProducer($json);
        $producer = ApplicationContext::getContainer()->get(Producer::class);
        $result = $producer->produce($message);
        var_dump($result);
        return $result;
    }
}