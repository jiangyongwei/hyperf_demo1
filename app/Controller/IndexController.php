<?php
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://hyperf.wiki
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */
namespace App\Controller;

use Hyperf\HttpServer\Annotation\Controller;
use Hyperf\HttpServer\Annotation\RequestMapping;
use Hyperf\Utils\ApplicationContext;
use Webmozart\Assert\Assert;
use App\Exception\FooException;
use App\Service\IndexService;

/**
 * 首页
 * @Controller(prefix="home/index")
 */
class IndexController extends AbstractController {
    /**
     * 首页
     * @author yongwei
     * @RequestMapping(path="index")
     */
    public function index() {

        /*$params['user_id'] = $this->checkUserToken();
        $service = new IndexService();
        $service->add([]);
        return $this->success(['name' => '江永威']);
        zlog('签名错误-sing-error', ['age' => 12]);*/
        //return $this->success();
        //throw new FooException(9000, 'stop');
        //abort(1001);
        /*$container = ApplicationContext::getContainer();
        $redis = $container->get(\Hyperf\Redis\Redis::class);
        //$redis->select(3);
        $result = $redis->set('db', '3');*/
        //$result = $redis->keys('*');
        //return $this->success(['name' => '江永威']);
        //123
        //456

        $user = $this->request->input('user', 'Hyperf');
        $method = $this->request->getMethod();
        $action = $this->request->getRequestUri();
        $url = $this->request->url();

        $data = [
            'method' => $method,
            'action' => $action,
            'url'    => $url,
            'message' => "Hello123456888 {$user}.",
            //'result' => $result,
            'params' => $this->request->all(),
        ];
        return $this->success($data);
    }

    /**
     * 定时任务测试
     * @author yongwei
     * @RequestMapping(path="crontab-test")
     */
    public function crontabTest() {
        var_dump(date('Y-m-d H:i:s', time()));
    }

    /**
     * 日志测试
     * @author yongwei
     * @RequestMapping(path="log-test")
     */
    public function logTest() {

    }

    /**
     * 爱的表白
     * @author yongwei
     * @RequestMapping(path="love")
     */
    public function love()
    {
        return [
            'text' => '永威爱雪婷',
        ];
    }


}
