<?php
namespace App\Controller;

use App\Service\QueueService;
use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpServer\Annotation\AutoController;
use Hyperf\HttpServer\Annotation\Controller;

/**
 * @AutoController
 */
class QueueController extends Controller
{
    /**
     * @Inject
     * @var QueueService
     */
    protected $service;

    /**
     * 传统模式投递消息
     */
    public function index()
    {
        $params = [
            'group@hyperf.io',
            'https://doc.hyperf.io',
            'https://www.hyperf.io',
        ];
        $delay = 3;
        $this->service->push($params, $delay);

        return 'success';
    }
}
