<?php
namespace App\Controller;

use App\Task\MethodTask;
use Hyperf\HttpServer\Annotation\AutoController;
use Hyperf\Utils\Coroutine;
use Hyperf\Utils\ApplicationContext;
use Hyperf\Task\TaskExecutor;
use Hyperf\Task\Task;

/**
 * @AutoController
 */
class TaskController {

    public function test()
    {
        $container = ApplicationContext::getContainer();
        $exec = $container->get(TaskExecutor::class);
        $result = $exec->execute(new Task([MethodTask::class, 'handle'], [Coroutine::id()]));
        return ['result' => $result];
    }


}
