<?php
namespace App\Controller;

use Hyperf\Guzzle\ClientFactory;
use Hyperf\HttpServer\Annotation\AutoController;

/**
 * @AutoController
 */
class GuzzleController {
    /**
     * @var \Hyperf\Guzzle\ClientFactory
     */
    private $clientFactory;

    public function __construct(ClientFactory $clientFactory)
    {
        $this->clientFactory = $clientFactory;
    }

    public function bar()
    {
        /*$arr = ['name' => '江永威', 'age' => 18];
        write_log('test', '123', $arr);
        return 'logtest';*/

        /*$arr = ['name' => '江永威', 'age' => 18];
        zlog('按日期格式保存日志到本地服务器', $arr);
        return 555;*/

        // $options 等同于 GuzzleHttp\Client 构造函数的 $config 参数
        $options = ['timeout' => 3];
        // $client 为协程化的 GuzzleHttp\Client 对象
        $client = $this->clientFactory->create($options);

        $method = 'POST';
        $action = $method == 'POST' ? 'form_params' : 'query';
        $url = 'https://app.chaoqunji.com';
        $data['AppKey'] = 'appkey';
        $data['TimeStamp'] = time() + 30;
        $data['Sign'] = 'sign';

        try {
            $response = $client->request($method, $url, [$action => $data]);
        } catch (\Exception $e) {
            var_dump($e->getMessage());
            //TODO邮箱报警
        }
        $code = $response->getStatusCode(); // 200  // 获取http状态码
        if ($code != 200) {
            //TODO 邮箱报警
        }
        $body = $response->getBody()->getContents(); //获取内容，将正文的其余内容作为字符串读取
        $arr = json_decode($body, true);
        var_dump($arr);
        /*\App\write('module_response_data', "{$module}-模块返回数据", $arr);
        if (!in_array($arr['ret'], [200])){
            //\App\abort(5000, "请求{$module}模块出错");
            \App\write('module_response_fail', "{$module}-模块请求出错", $arr);
            return false;
        }
        return $arr['data'];*/
        return $arr;
    }

    public function yichang() {
        //$c = new ABC();
        $a = $b;
        //abort(999, "报错啦");
        //$a = $b;

        return 456;
    }

    public function test() {
        return 888;
    }
}
