<?php

declare(strict_types=1);

namespace App\Amqp\Consumer;

use Hyperf\Amqp\Result;
use Hyperf\Amqp\Annotation\Consumer;
use Hyperf\Amqp\Message\ConsumerMessage;
use PhpAmqpLib\Message\AMQPMessage;

/*禁止消费进程自启
默认情况下，使用了 @Consumer 注解后，框架会自动创建子进程启动消费者，并且会在子进程异常退出后，重新拉起。 如果出于开发阶段，进行消费者调试时，可能会因为消费其他消息而导致调试不便。
这种情况，只需要在 @Consumer 注解中配置 enable=false (默认为 true 跟随服务启动)或者在对应的消费者中重写类方法 isEnable() 返回 false 即可
返回值	行为
\Hyperf\Amqp\Result::ACK	确认消息正确被消费掉了
\Hyperf\Amqp\Result::NACK	消息没有被正确消费掉，以 basic_nack 方法来响应
\Hyperf\Amqp\Result::REQUEUE	消息没有被正确消费掉，以 basic_reject 方法来响应，并使消息重新入列
\Hyperf\Amqp\Result::DROP	消息没有被正确消费掉，以 basic_reject 方法来响应
*/

/**
 * DemoConsumer 消费消息
 * @Consumer(exchange="hyperf", routingKey="hyperf", queue="hyperf", name ="DemoConsumer", nums=1, enable=true)
 */
/*class DemoConsumer extends ConsumerMessage
{
    public function consumeMessage($data, AMQPMessage $message): string
    {
        var_dump($data);
        return Result::ACK;
    }


}*/
