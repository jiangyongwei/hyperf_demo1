<?php

declare(strict_types=1);

namespace App\Amqp\Producer;

use Hyperf\Amqp\Annotation\Producer;
use Hyperf\Amqp\Message\ProducerMessage;

/**
 * https://www.hyperf.wiki/2.1/#/zh-cn/amqp
 * DemoProducer 投递消息
 * @Producer(exchange="hyperf", routingKey="hyperf")
 */
/*class DemoProducer extends ProducerMessage
{
    // 其中 payload 就是最终投递到消息队列中的数据，所以我们可以随意改写 __construct 方法，只要最后赋值 payload 即可
    public function __construct($data)
    {
        $this->payload = $data;
    }
}*/
