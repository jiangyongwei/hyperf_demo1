<?php
namespace App\Exception;

use Hyperf\Server\Exception\ServerException;
use Throwable;

class FooException extends ServerException
{
    public function __construct(int $code = 0, string $message = null, Throwable $previous = null)
    {
        $errorArr = config('error_code');
        if (is_null($message)) {
            $message = $errorArr[$code];
        }

        parent::__construct($message, $code, $previous);
    }
}
