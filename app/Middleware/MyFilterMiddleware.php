<?php
declare(strict_types=1);

namespace App\Middleware;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;

class MyFilterMiddleware implements MiddlewareInterface {

    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        return json();
        $allParams = $request->all();
        zlog('request_params:请求参数', $allParams);
        $action = $request->getRequestUri(); //获取访问的文件路径 http://192.168.31.163:9501/home/index/index?age=1&name=jiang  => /home/index/index
        if(! isset($allParams['sign']) || empty($allParams['sign'])){
            return json([], 0, '签名参数错误');
        }

        return $handler->handle($request);
    }

}